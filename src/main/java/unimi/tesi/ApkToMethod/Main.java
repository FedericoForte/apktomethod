package unimi.tesi.ApkToMethod;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.android.dex.Dex;
import com.android.dex.MethodId;

public class Main {

	public static void main(String[] args) {
		DexReader dexRd;
		Dex classesDex;
		List<MethodId> listMethod;
		MethodHandler handler;
		byte[] data;
		Pair p = extractOptions(args);
		FilterMethod filter= p.filter;
		String[] listArg = p.listArg;
		String methodOutput;
		try {
			// all apk passed by args except option
			for (String fileName : listArg) {
				dexRd = new DexReader(new ApkReader(fileName));
				data = dexRd.readDex();
				classesDex = new Dex(data);
				listMethod = classesDex.methodIds();
				for (MethodId m : listMethod) {
					handler = new MethodHandler(classesDex, m);
					methodOutput = handler.createMethod(filter);
					// construct method human-readable and print it
					if(!methodOutput.equals(""))
						System.out.println(methodOutput);
				}

			}
		} catch (IllegalArgumentException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			if (e.getMessage().equals("This file hasn't apk extension!"))
				System.out.println(e.getMessage());
			else
				System.out.println("Error");
		}

	}
	
	//created for function extractOptions
	protected static class Pair{
		public FilterMethod filter;
		public String[] listArg;
		
		public Pair(FilterMethod f, String[] array){
			filter = f;
			listArg = array;
		}
	}

	// return all apk and the right filter setted
	protected static Pair extractOptions(String[] args) {
		if (args.length > 1) {
			if (args[0].equals("--CryptoNetwork"))
				return new Pair(new FilterCryptoNetworkMethod(), Arrays.copyOfRange(args, 1, args.length));
			else if (args[0].equals("--onlyNetwork"))
				return new Pair(new FilterNetworkMethod(), Arrays.copyOfRange(args, 1, args.length));
			else if (args[0].equals("--onlyCrypto"))
				return new Pair(new FilterCryptoMethod(), Arrays.copyOfRange(args, 1, args.length));
		}
		return new Pair(new NoFilterMethod(), args);
	}

}
