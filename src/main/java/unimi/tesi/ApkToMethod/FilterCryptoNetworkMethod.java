package unimi.tesi.ApkToMethod;

public class FilterCryptoNetworkMethod extends FilterMethod{
	
	protected static final String[] CRYPTO_ALGORITHM_CASE_SENSITIVE = createPattern(
			new String[]{"AES", "RSA", "DES", "DSA", "TTH", "ECC", "PES", "IDEA", "MMB", 
					"MESH", "TEA", "SHA"}, false);
	
	protected static final String[] CRYPTO_ALGORITHM_NO_CASE_SENSITIVE = createPattern( 
			new String[]{"Blowfish", "Twofish", "Threefish", "TigerThreeHash", 
					"MD4", "MD5", "HMAC", "rc2", "rc4", "rc5", "rc6",
					"RIPEMD", "RTR0", "NTRUEncrypt", "Akelarre", "WHIRLPOOL", "PKCS"}, true);
	
	protected static final String[] NETWORK_CASE_SENSITIVE = createPattern(
			new String[]{"SSL","TLS","SSH"}, false);
	
	protected static final String[] NETWORK_NO_CASE_SENSITIVE = createPattern(
			new String[]{"http", "X509", "X.509","Telnet"}, true); 
	
	protected static final String[] OTHER_KEY_WORD = createPattern(
			new String[]{"crypto", "cipher", "Blakey", "Shamir", "ElGamal", "elliptic", 
					"encrypt", "decrypt", "Diffie", "Hellman", "secure", 
					"symmetric", "asymmetric", "security", "secret"}, true);
	
	public String applyFilter(String packageName, String method, String params, String returnType){
		for(String s: CRYPTO_ALGORITHM_CASE_SENSITIVE)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return "CryptoAlgo: " + joinMethodParts(packageName, method, params, returnType);
		for(String s: CRYPTO_ALGORITHM_NO_CASE_SENSITIVE)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return "CryptoAlgo: " + joinMethodParts(packageName, method, params, returnType);
		for(String s: NETWORK_CASE_SENSITIVE)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return "Network: " + joinMethodParts(packageName, method, params, returnType);
		for(String s: NETWORK_NO_CASE_SENSITIVE)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return "Network: " + joinMethodParts(packageName, method, params, returnType);
		for(String s: OTHER_KEY_WORD)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return "Other: " + joinMethodParts(packageName, method, params, returnType);	
		return "";
	}
	
	private static String[] createPattern(String[] array, boolean insensitive){
		String[] res = new String[array.length];
		for(int i=0; i < array.length; i++)
			if(insensitive)
				res[i] = "(?i).*" + array[i] + ".*";
			else
				res[i] = ".*" + array[i] + ".*";
		return res;
		
	}


}
