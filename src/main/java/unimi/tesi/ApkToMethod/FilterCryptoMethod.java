package unimi.tesi.ApkToMethod;

public class FilterCryptoMethod extends FilterCryptoNetworkMethod{
	
	public String applyFilter(String packageName, String method, String params, String returnType){
		for(String s: CRYPTO_ALGORITHM_CASE_SENSITIVE)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return joinMethodParts(packageName, method, params, returnType);
		for(String s: CRYPTO_ALGORITHM_NO_CASE_SENSITIVE)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return joinMethodParts(packageName, method, params, returnType);
		for(String s: OTHER_KEY_WORD)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return joinMethodParts(packageName, method, params, returnType);
		return "";
	}
	
}
