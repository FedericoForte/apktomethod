package unimi.tesi.ApkToMethod;

import com.android.dex.Dex;
import com.android.dex.MethodId;
import com.android.dex.ProtoId;

public class MethodHandler {

	private Dex file;
	private MethodId method;

	public MethodHandler(Dex file, MethodId method) {
		this.file = file;
		this.method = method;
	}

	// lastElement = true return only last part es. com/android/Dex -> Dex
	protected static String humanReadable(String type, boolean lastElement) {
		// [ -> array
		if (type.startsWith("[")) {
			return humanReadable(type.substring(1), lastElement) + "[]";
		}
		// (L -> begin) (; -> end)
		if (type.startsWith("L")) {
			String name = type.substring(1, type.length() - 1);
			if (lastElement) {
				String[] array = name.split("/");
				return array[array.length - 1];
			}
			//package name sep by .
			return name.replace("/", ".");
		}

		switch (type) {
		case "B":
			return "byte";
		case "C":
			return "char";
		case "D":
			return "double";
		case "F":
			return "float";
		case "I":
			return "int";
		case "J":
			return "long";
		case "S":
			return "short";
		case "V":
			return "void";
		case "Z":
			return "boolean";
		default:
			throw new IllegalArgumentException("Unknown type: " + type);
		}
	}
	
	public String createMethod(FilterMethod filter){
		 String packageName = humanReadable(file.typeNames().get(method.getDeclaringClassIndex()), false);
		 String methodName = file.strings().get(method.getNameIndex());
		 ProtoId methodProtoIds = file.protoIds().get(method.getProtoIndex());
		 short[] paramsArray = file.readTypeList(methodProtoIds.getParametersOffset()).getTypes();
		 String params= "";
		 for(short s: paramsArray)
			 params = params + humanReadable(file.typeNames().get(s), true) + ", ";
		 if(params.length()>= 3)
			 params = params.substring(0, params.length() - 2);
		 String returnType = humanReadable(file.typeNames().get(methodProtoIds.getReturnTypeIndex()), true);
		 return filter.applyFilter(packageName, methodName, params, returnType);
		 
	}

}
