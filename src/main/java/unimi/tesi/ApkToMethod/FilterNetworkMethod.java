package unimi.tesi.ApkToMethod;

public class FilterNetworkMethod extends FilterCryptoNetworkMethod{
	
	public String applyFilter(String packageName, String method, String params, String returnType){
		for(String s: NETWORK_CASE_SENSITIVE)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return joinMethodParts(packageName, method, params, returnType);
		for(String s: NETWORK_NO_CASE_SENSITIVE)
			if(packageName.matches(s) || method.matches(s) || params.matches(s) || returnType.matches(s))
				return joinMethodParts(packageName, method, params, returnType);	
		return "";
	}
	
}
