package unimi.tesi.ApkToMethod;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;

public class DexReader {
	
	private ApkReader apk;
	
	private static final String CLASSES_DEX = "classes.dex";
	
	private static final byte[] DEX_MAGIC = new byte[] { 0x64, 0x65, 0x78, 0x0a, 0x30, 0x33, 0x35, 0x00 };
	
	public DexReader(ApkReader apkRd){
		this.apk=apkRd;
	}
	
	public byte[] readDex() throws IOException{
		 Enumeration<? extends ZipEntry> entries = apk.getApkEntries();
		 ZipEntry entry;
		 byte[] data;
		 while(entries.hasMoreElements()){
			 entry = entries.nextElement();
		     if(entry.getName().equals(CLASSES_DEX)){
		    	 data = readFromStream(apk.getInputStream(entry));
		    	 if(startsWith(data, DEX_MAGIC))
		    		 return data;
		    	 else
		    		 throw new IllegalArgumentException("Dex not valid");
		     } 
		 }
		 throw new IllegalArgumentException("Apk not valid");
		
		
	}
	
	public static boolean checkValidDex(byte[] b){
		return startsWith(b, DEX_MAGIC);
	}
	
	private static byte[] readFromStream(InputStream in) throws IOException{
		int available = in.available();  
        byte[] data = new byte[ available ];  
        new DataInputStream(in).readFully(data);
        in.close();
        return data;
	}
	
	private static boolean startsWith(byte[] first, byte[] second) {
		if(first.length < second.length)
			return false;
		for (int i = 0; i < second.length; i++)
			if (first[i] != second[i])
				return false;
		return true;
	}

}
