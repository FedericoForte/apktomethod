package unimi.tesi.ApkToMethod;

import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ApkReader {
	
	private ZipFile apkFile;

	public ApkReader(String nameApk) throws IOException{
		if(nameApk.endsWith(".apk"))
			apkFile = new ZipFile(nameApk);
		else
			throw new IOException("This file hasn't apk extension!");
	}
	
	public Enumeration<? extends ZipEntry> getApkEntries(){
		return apkFile.entries();
	}
	
	public InputStream getInputStream(ZipEntry entry) throws IOException{
		return apkFile.getInputStream(entry);
	}
	
	
	
}
