package unimi.tesi.ApkToMethod;

public class NoFilterMethod extends FilterMethod{
	
	public String applyFilter(String packageName, String method, String params, String returnType){
		return joinMethodParts(packageName, method, params, returnType);
	}

}
