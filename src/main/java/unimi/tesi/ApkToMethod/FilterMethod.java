package unimi.tesi.ApkToMethod;

public abstract class FilterMethod {
	
	protected static String joinMethodParts(String packageName, String method, String params, String returnType){
		if (returnType == "void") {
			 return packageName +  " " + method + "(" + params + ")";
		 }
		 return packageName +  " " + method + "(" + params + ") -> " + returnType;
	} 
	
	public abstract String applyFilter(String packageName, String method, String params, String returnType);
}
