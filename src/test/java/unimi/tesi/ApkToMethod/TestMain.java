package unimi.tesi.ApkToMethod;

import static org.junit.Assert.*;

import org.junit.Test;

import unimi.tesi.ApkToMethod.Main.Pair;

public class TestMain {

	@Test
	public void testExtractOptions() {
		String[] args = new String[] { "--Crypto&Network", "file.apk", "file2.apk" };
		Pair p = Main.extractOptions(args);
		assertEquals(args.length - 1, p.listArg.length);
		assertTrue(p.filter instanceof FilterCryptoNetworkMethod);

	}

	@Test
	public void testExtractOptionsNetwork() {
		String[] args = new String[] { "--onlyNetwork", "file.apk", "file2.apk" };
		Pair p = Main.extractOptions(args);
		assertEquals(args.length - 1, p.listArg.length);
		assertTrue(p.filter instanceof FilterNetworkMethod);

	}

	@Test
	public void testExtractOptionsCrypto() {
		String[] args = new String[] { "--onlyCrypto", "file.apk", "file2.apk" };
		Pair p = Main.extractOptions(args);
		assertEquals(args.length - 1, p.listArg.length);
		assertTrue(p.filter instanceof FilterCryptoMethod);
	}

	@Test
	public void testExtractOptionsNofilter() {
		String[] args = new String[] { "file.apk", "file2.apk" };
		Pair p = Main.extractOptions(args);
		assertEquals(args.length, p.listArg.length);
		assertTrue(p.filter instanceof NoFilterMethod);
	}

	@Test
	public void testExtractOptionsNofilterOneElement() {
		String[] args = new String[] { "file.apk" };
		Pair p = Main.extractOptions(args);
		assertEquals(args.length, p.listArg.length);
		assertTrue(p.filter instanceof NoFilterMethod);
	}

}
