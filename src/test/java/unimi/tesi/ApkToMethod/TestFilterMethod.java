package unimi.tesi.ApkToMethod;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestFilterMethod {
	
	@Test
	public void testFilterCrypto(){
		FilterMethod f = new FilterCryptoNetworkMethod();
		assertEquals("Other: com.javax.crypto.* generateKey(int,String)",f.applyFilter("com.javax.crypto.*", "generateKey", "int,String", "void"));
		assertEquals("",f.applyFilter("com.javax.*", "generate", "int,String", "void"));
		assertEquals("CryptoAlgo: com.mypackage.util getInstanceRSA(SecretKey) -> int",f.applyFilter("com.mypackage.util", "getInstanceRSA", "SecretKey", "int"));
	}
	
	@Test
	public void testFilterCryptoNetwork(){
		FilterMethod f = new FilterCryptoNetworkMethod();
		assertEquals("Network: java.util.* createConn(int,String) -> SSL",f.applyFilter("java.util.*", "createConn", "int,String", "SSL"));
		assertEquals("Network: com.javax.* httpsRequest()",f.applyFilter("com.javax.*", "httpsRequest", "", "void"));
		
	}
	
	@Test
	public void testNoFilter(){
		FilterMethod f = new NoFilterMethod();
		assertEquals("com.javax.crypto.* generateKey(int,String) -> List<String>",f.applyFilter("com.javax.crypto.*", "generateKey", "int,String", "List<String>"));
		assertEquals("com.javax.* generate(int,String)",f.applyFilter("com.javax.*", "generate", "int,String", "void"));
		assertEquals("com.mypackage.util getInstance(SecretKey) -> int",f.applyFilter("com.mypackage.util", "getInstance", "SecretKey", "int"));
	}
	
	@Test
	public void testOnlyFilterNetwork(){
		FilterMethod f = new FilterNetworkMethod();
		assertEquals("",f.applyFilter("com.javax.crypto.*", "generateKey", "int,String", "List<String>"));
		assertEquals("com.javax.* httpsRequest()",f.applyFilter("com.javax.*", "httpsRequest", "", "void"));
		assertEquals("com.mypackage.util getInstance(SSL) -> int",f.applyFilter("com.mypackage.util", "getInstance", "SSL", "int"));
	}
	
	@Test
	public void testOnlyFilterCrypto(){
		FilterMethod f = new FilterCryptoMethod();
		assertEquals("com.javax.crypto.* DES(int,String) -> List<String>",f.applyFilter("com.javax.crypto.*", "DES", "int,String", "List<String>"));
		assertEquals("",f.applyFilter("com.javax.*", "httpsRequest", "", "void"));
		assertEquals("",f.applyFilter("com.mypackage.util", "getInstance", "SSL", "int"));
	}
}
