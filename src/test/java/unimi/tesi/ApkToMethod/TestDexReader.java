package unimi.tesi.ApkToMethod;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

public class TestDexReader {
	
	@Test
	public void testDexExecution() throws IOException{
		ApkReader apk = new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/OrarioTreni.apk");
		DexReader dex = new DexReader(apk);
		byte[] data = dex.readDex();
		assertTrue(data!=null);	
	}
	
	@Test
	public void testDexException() throws IOException{
		ApkReader apk = new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/TestFake.apk");
		DexReader dex = new DexReader(apk);
		try{
			dex.readDex();
			fail();
		}catch(IllegalArgumentException e){
			assertEquals(e.getMessage(), "Apk not valid");
			
		}	
		
	}
	
	@Test
	public void testDexException2() throws IOException{
		ApkReader apk = new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/TestFakeDex.apk");
		DexReader dex = new DexReader(apk);
		try{
			dex.readDex();
			fail();
		}catch(IllegalArgumentException e){
			assertEquals(e.getMessage(), "Dex not valid");
			
		}	
		
	}
	
	@Test
	public void testDexNotEmptyException() throws IOException{
		ApkReader apk = new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/TestFakeDex2.apk");
		DexReader dex = new DexReader(apk);
		try{
			dex.readDex();
			fail();
		}catch(IllegalArgumentException e){
			assertEquals(e.getMessage(), "Dex not valid");
			
		}	
		
	}
	

}
