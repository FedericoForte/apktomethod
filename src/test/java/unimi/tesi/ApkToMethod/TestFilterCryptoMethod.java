package unimi.tesi.ApkToMethod;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestFilterCryptoMethod {
	
	@Test
	public void testFilterCrypto(){
		FilterMethod f = new FilterCryptoMethod();
		assertEquals("com.javax.crypto.* generateKey(int,String)",f.applyFilter("com.javax.crypto.*", "generateKey", "int,String", "void"));
		assertEquals("",f.applyFilter("com.javax.*", "generate", "int,String", "void"));
		assertEquals("com.mypackage.util getInstance(SecretKey) -> int",f.applyFilter("com.mypackage.util", "getInstance", "SecretKey", "int"));
	}

}
