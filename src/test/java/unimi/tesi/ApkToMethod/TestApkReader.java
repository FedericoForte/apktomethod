package unimi.tesi.ApkToMethod;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;

import org.junit.Test;

public class TestApkReader {
	
	@Test
	public void testConstructor() throws IOException{
		ApkReader rd = new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/OrarioTreni.apk");
		assertTrue(rd!=null);
	}
	
	@Test(expected = IOException.class)
	public void testConstructorException() throws IOException{
		new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/TestEmptyZip.zip");
	}
	
	@Test
	public void testConstructorException2() throws IOException{
		try{
			new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/TestFile.txt");
			fail();
		}catch(IOException e){
			assertEquals(e.getMessage(), "This file hasn't apk extension!");
			
		}
	}
	
	@Test
	public void testgetApkEntries() throws IOException{
		ApkReader rd = new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/OrarioTreni.apk");
		assertTrue(rd.getApkEntries()!=null);
	}
	
	@Test
	public void testInputStream() throws IOException{
		ApkReader rd = new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/OrarioTreni.apk");
		Enumeration<? extends ZipEntry> entries = rd.getApkEntries();
		ZipEntry entry = entries.nextElement();
		assertTrue(rd.getInputStream(entry)!=null);
	}
	
	
}
