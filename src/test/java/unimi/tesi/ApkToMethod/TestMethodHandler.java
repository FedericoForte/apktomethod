package unimi.tesi.ApkToMethod;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

import com.android.dex.Dex;
import com.android.dex.MethodId;

public class TestMethodHandler {
	
	@Test
	public void testHumanReadable(){
		String s1 = "Lcom/orariotrenifree/orariotreni/Information;";
		String s2 = "[J";
		String s3 = "[Ljava/lang/reflect/Type;";
		String s4 = "Z";
		assertEquals("com.orariotrenifree.orariotreni.Information",MethodHandler.humanReadable(s1, false));
		assertEquals("long[]",MethodHandler.humanReadable(s2, false));
		assertEquals("java.lang.reflect.Type[]",MethodHandler.humanReadable(s3, false));
		assertEquals("boolean",MethodHandler.humanReadable(s4, false));
	}
	
	@Test
	public void testHumanReadableTrue(){
		String s1 = "Lcom/orariotrenifree/orariotreni/Information;";
		String s2 = "[J";
		String s3 = "[Ljava/lang/reflect/Type;";
		String s4 = "Z";
		assertEquals("Information",MethodHandler.humanReadable(s1, true));
		assertEquals("long[]",MethodHandler.humanReadable(s2, true));
		assertEquals("Type[]",MethodHandler.humanReadable(s3, true));
		assertEquals("boolean",MethodHandler.humanReadable(s4, true));
	}
	
	@Test
	public void testCreateMethod() throws IOException{
		ApkReader apk = new ApkReader("src/test/java/unimi/tesi/ApkToMethod/FilesForTesting/OrarioTreni.apk");
		DexReader dex = new DexReader(apk);
		byte[] data = dex.readDex();
		Dex dexFile = new Dex(data);
		List<MethodId> listMethod = dexFile.methodIds();
		MethodHandler handler;
		FilterMethod f = new NoFilterMethod();
		for(MethodId m : listMethod){
			handler= new MethodHandler(dexFile, m);
			assertTrue(handler.createMethod(f).matches(".* .*(.*)( -> .*)?"));
		}
	}

}
